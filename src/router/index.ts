// @ts-ignore
import {createRouter, createRouterMatcher, createWebHashHistory} from "vue-router";
import home from "../components/Home.vue";
import Foo from "../components/Foo.vue";
import Login from "../components/Login.vue";
// const Foo = { template: '<div>foo</div>'}

const routes = [
    {
      path: "/",
      redirect: '/foo'
    },
    {
        path: "/home",
        name: "home",
        component: home
    },
    {
        path: '/foo',
        name: "foo",
        component: Foo
    },
    {
        path: '/Login',
        name: 'login',
        component: Login
    }
]

// @ts-ignore
export const router = createRouter({
    history: createWebHashHistory(),
    routes: routes
})