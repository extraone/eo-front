import { createApp } from 'vue'
import './style.css'
import App from './App.vue'
import {router} from "./router";
// @ts-ignore
import axios from 'axios';
import VueAxios from 'vue-axios';

createApp(App)
    .use(router)
    .use(VueAxios, axios)
    // .use(VueCookies)
    .mount('#app')