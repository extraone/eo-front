import {createStore} from "vuex";

const store = createStore({
    state: {
        userInfo:{
            userID: '',
            userName: 'userName',
            cachedMenu: [
                {
                    path: "/Home",
                }
            ]
        }
    }
})